﻿using Carpooling.Services.DTOs;
using System.Collections.Generic;

namespace Carpooling.Web.Models.APIModel
{
    public class UserResponseModel
    {
        public UserResponseModel(UserPresentDTO userPresentDTO)
        {
            this.Id = userPresentDTO.Id;
            this.Username = userPresentDTO.Username;
            this.FirstName = userPresentDTO.FirstName;
            this.LastName = userPresentDTO.LastName;
            this.Email = userPresentDTO.Email;
            this.PhoneNumber = userPresentDTO.PhoneNumber;
            this.ProfilePic = userPresentDTO.ProfilePic;
            this.Status = userPresentDTO.Status.ToString();
            this.Roles = userPresentDTO.Roles;
            this.RatingAsDriver = userPresentDTO.RatingAsDriver;
            this.RatingAsPassenger = userPresentDTO.RatingAsPassenger;
        }
        public int Id { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string ProfilePic { get; set; }

        public double RatingAsDriver { get; set; }

        public double RatingAsPassenger { get; set; }

        public string Status { get; set; }

        public IEnumerable<string> Roles { get; set; }

        public string Password { get; set; }
    }
}
