﻿using Carpooling.Services.DTOs;

namespace Carpooling.Web.Models.APIModel
{
    public class FeedbackResponseModel
    {
        public FeedbackResponseModel(FeedbackPresentDTO feedbackPresentDTO, UserResponseModel userFrom, UserResponseModel userTo)
        {
            this.Rating = feedbackPresentDTO.Rating;
            this.Type = feedbackPresentDTO.Type.ToString();
            this.UserFrom = userFrom;
            this.UserTo = userTo;
            this.TravelId = feedbackPresentDTO.TravelId;
            this.Comment = feedbackPresentDTO.Comment;
        }

        public double Rating { get; set; }

        public string Comment { get; set; }

        public string Type { get; set; }

        public UserResponseModel UserFrom { get; set; }

        public UserResponseModel UserTo { get; set; }

        public int TravelId { get; set; }
    }
}
