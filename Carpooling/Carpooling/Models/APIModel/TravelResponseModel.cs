﻿using Carpooling.Services.DTOs;
using System;
using System.Collections.Generic;

namespace Carpooling.Web.Models.APIModel
{
    public class TravelResponseModel
    {
        public TravelResponseModel(TravelPresentDTO travelPresentDTO, CityResponseModel startAddressResponseModel,
                                  CityResponseModel endAddressResponseModel, IEnumerable<UserResponseModel> passengers)
        {
            this.TravelId = travelPresentDTO.Id;
            this.StartPointAddress = startAddressResponseModel;
            this.EndPointAddress = endAddressResponseModel;
            this.DepartureTime = travelPresentDTO.DepartureTime;
            this.FreeSpots = travelPresentDTO.FreeSpots;
            this.IsCompleted = travelPresentDTO.IsCompleted;
            this.DriverFirstName = travelPresentDTO.Driver.FirstName;
            this.DriverLastName = travelPresentDTO.Driver.LastName;
            this.Passengers = passengers;
            this.TravelTags = travelPresentDTO.TravelTags;
        }
        public int TravelId { get; }

        public CityResponseModel StartPointAddress { get; set; }

        public CityResponseModel EndPointAddress { get; set; }

        public string DriverFirstName { get; set; }

        public string DriverLastName { get; set; }

        public DateTime DepartureTime { get; set; }

        public int FreeSpots { get; set; }

        public bool IsCompleted { get; set; }

        public IEnumerable<UserResponseModel> Passengers { get; set; }

        public IEnumerable<string> TravelTags { get; set; }

    }
}
