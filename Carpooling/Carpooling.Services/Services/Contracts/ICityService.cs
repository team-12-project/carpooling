﻿using Carpooling.Data.Models;

namespace Carpooling.Services.Services.Contracts
{
    public interface ICityService
    {
        public City CreateCity(string city);

        public City CheckIfCityExist(string city);
    }
}
