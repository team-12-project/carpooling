﻿namespace Carpooling.Services.DTOs
{
    public class CityCreateDTO
    {
        public string City { get; set; }
    }
}
